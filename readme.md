Asset generation for squadmortar.xyz

Generates heightmaps in correct pixel format and converts minimaps from .bmp to .jpg with appropriate quality. 
Assets should be taken from the Squad SDK and put into an 'input' folder at <cwd>/input. 
Likewise a folder at <cwd>/output is required. Heightmap names should be '<name>_heightmap.png', minimap names should be '<name>_minimap.bmp'.
A prototype implementation for contour maps is also included, but these are not used in squadmortar 
and so are currently not generated.

