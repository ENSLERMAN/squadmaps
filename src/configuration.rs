use ron::*;
use serde::*;
use std::collections::HashMap;
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Landscape{
    pub loc_x: f64,
    pub loc_y: f64,
    pub scale_x: f64,
    pub scale_y: f64,
    pub scale_z: f64,
    pub resolution_x: u32,
    pub resolution_y: u32,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Size2D {
    pub size_x: u32,
    pub size_y: u32
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Location2D{
    pub loc_x: f64,
    pub loc_y: f64
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Level{
    pub landscape: Landscape,
    pub map_texture: Size2D,
    pub map_texture_corner_0: Location2D,
    pub map_texture_corner_1: Location2D,
}

pub type Levels<'a> = HashMap<&'a str, Level>;