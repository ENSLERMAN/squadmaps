//#![feature(bindings_after_at)]
//#![feature(int_roundings)]
#![allow(warnings, unused)]
extern crate image;
extern crate oxipng;

use image::{ImageError, DynamicImage, ImageBuffer, Luma, GenericImageView, Rgb, RgbaImage, Rgba, RgbImage, EncodableLayout};
use image::imageops::filter3x3;
use num::range;
use rayon::prelude::*;
use num::integer::sqrt;
use serde::{Deserialize, Serialize};
use serde_json::{Result as JsonResult};
mod configuration;
use configuration::*;
mod scalar_field;
use scalar_field::*;
mod square;
use square::*;
mod drawing;
use drawing::*;
mod simplify;
use simplify::*;
use ron::from_str;
use std::fs;
use std::path::PathBuf;
use std::fmt::Error;
use oxipng::InFile;

fn main() -> Result<(), String> {
  /*
  let ron_string = fs::read_to_string("src/levels.ron").expect("Something went wrong reading the level configuration file");
  let level_configurations:Levels = from_str(&ron_string).map_err(|err| format!("{}:{}", err.position, err.to_string()))?;
  let contourmap_jobs = get_contourmap_job_details()?;
  for (name, in_path, out_path) in contourmap_jobs {
    let (in_str, out_str) = (in_path.to_str().ok_or("...")?, out_path.to_str().ok_or("...")?);
    let scale_z = level_configurations.get(&name as &str).ok_or(format!("lookup failed for {}", name))?.landscape.scale_z;
    create_svg(
      1,
      u16::MAX,
      100.0 * (100.0 / scale_z),
      in_str,
      out_str
    );
  }
   */
  let heightmap_jobs = get_heightmap_job_details()?;
  for (name2, in_path2, out_path2) in heightmap_jobs {
    let img = image::open(in_path2).map_err(|err|  "could not open heightmap image")?.into_luma16();
    let w = img.width();
    let h = img.height();
    //let output_img: RgbImage = ImageBuffer::new(img.width(), img.height());
    let mut out_data = vec![];
    for value in img.into_raw() {
      let (r, g, b) = pixel_conversion(value);
      out_data.push(r);
      out_data.push(g);
      out_data.push(b);
    }
    let out_img: RgbImage = ImageBuffer::from_vec(w, h, out_data).ok_or("failed to create imagebuffer")?;
    out_img.save(out_path2.clone());
    let mut oxipng_options = oxipng::Options::default();
    oxipng_options.force = true;
    oxipng_options.preserve_attrs = true;
    oxipng_options.bit_depth_reduction = false;
    oxipng::optimize(&oxipng::InFile::Path(out_path2.clone()), &oxipng::OutFile::Path(Some(out_path2.clone())), &oxipng_options);
    //let optimized = oxipng::optimize_from_memory(out_data.as_bytes(), &oxipng_options).map_err(|err| format!("failed to optimize heightmap: {}", err.to_string()))?;

  }

  let minimap_jobs = get_minimap_job_details()?;
  for (name3, in_path3, out_path3) in minimap_jobs {
    let (in_str, out_str) = (in_path3.to_str().ok_or("...")?, out_path3.to_str().ok_or("...")?);
    bmp_to_jpg_with_quality(in_str, out_str, 70);
  }
  Ok(())
}

fn pixel_conversion(pixel: u16) -> (u8, u8, u8){
  let r = (pixel >> 8) as u8;
  let g = (pixel & 0x00FF) as u8;
  (r, g, 0)
}
#[cfg(test)]
#[test]
fn test_pixel_conversion() {
  assert_eq!(0xFF, u8::MAX);
  assert_eq!(pixel_conversion((0x1234)), (0x12, 0x34, 0x0));
  assert_eq!(pixel_conversion((0xFFFF)), (0xFF, 0xFF, 0x0));
  assert_eq!(pixel_conversion((0x0000)), (0x00, 0x00, 0x0));
  assert_eq!(pixel_conversion((0xFF00)), (0xFF, 0x00, 0x0));
  assert_eq!(pixel_conversion((0x00FF)), (0x00, 0xFF, 0x0));
  assert_eq!(pixel_conversion((0x01FF)), (0x01, 0xFF, 0x0));
}

fn get_contourmap_job_details() -> Result<Vec<(String, PathBuf, PathBuf)>, String> {
  let dir_entries = fs::read_dir("./input").map_err(|err| err.to_string())?;
  let mut out_paths = vec![];
  for dir_entry in dir_entries {
    let mut path_buf = dir_entry.map_err(|err|err.to_string())?.path();
    let file_stem = path_buf.file_stem().ok_or("could not get file_stem")?.to_str().ok_or("could not convert file_stem to_str")?;
    if file_stem.contains("heightmap"){
      let name = *file_stem.split("_").collect::<Vec<&str>>().get(0).ok_or("no name")?;
      let new_filename = format!("{}{}", name, "_contour.svg");
      let out_path: PathBuf = [".", "output", &new_filename].iter().collect();

      out_paths.push((name.to_owned(), path_buf, out_path))
    }
  }
  Ok(out_paths)
}

fn get_heightmap_job_details() -> Result<Vec<(String, PathBuf, PathBuf)>, String> {
  let dir_entries = fs::read_dir("./input").map_err(|err| err.to_string())?;
  let mut out_paths = vec![];
  for dir_entry in dir_entries {
    let mut path_buf = dir_entry.map_err(|err|err.to_string())?.path();
    let file_stem = path_buf.file_stem().ok_or("could not get file_stem")?.to_str().ok_or("could not convert file_stem to_str")?;
    if file_stem.contains("heightmap"){
      let name = *file_stem.split("_").collect::<Vec<&str>>().get(0).ok_or("no name")?;
      let new_filename = format!("{}{}", name, "_heightmap.png");
      let out_path: PathBuf = [".", "output", &new_filename].iter().collect();

      out_paths.push((name.to_owned(), path_buf, out_path))
    }
  }
  Ok(out_paths)
}

fn get_minimap_job_details() -> Result<Vec<(String, PathBuf, PathBuf)>, String> {
  let dir_entries = fs::read_dir("./input").map_err(|err| err.to_string())?;
  let mut out_paths = vec![];
  for dir_entry in dir_entries {
    let mut path_buf = dir_entry.map_err(|err|err.to_string())?.path();
    let file_stem = path_buf.file_stem().ok_or("could not get file_stem")?.to_str().ok_or("could not convert file_stem to_str")?;
    if file_stem.contains("minimap"){
      let new_filename = format!("{}{}", file_stem, ".jpg");
      let out_path: PathBuf = [".", "output", &new_filename].iter().collect();
      let name = *file_stem.split("_").collect::<Vec<&str>>().get(0).ok_or("no name")?;
      out_paths.push((name.to_owned(), path_buf, out_path))
    }
  }
  Ok(out_paths)
}

fn create_svg(min_height: u16, max_height: u16, step_size: f64, in_path: &str, out_path: &str) -> Result<(), ImageError> {
  let img = image::open(in_path)?.into_luma16();
  let field = ScalarField {
    values: img.as_raw(),
    column_len: img.height(),
    row_len: img.width(),
  };

  let (w, h) = (field.row_len.clone(), field.column_len.clone());
  let max = *field.values.iter().filter(|v| **v <= max_height && **v >= min_height).max().unwrap();
  let min = *field.values.iter().filter(|v|**v <= max_height && **v >= min_height).min().unwrap();
  let diff = max - min;
  let step_count = diff as f64 / step_size;
  let paths: Vec<(String, Line)> = (0..(step_count as u16)).into_par_iter().map(|i| {
    //let color_range = vec![((min as f64) as f64, (0, 0, 0)), ((max as f64)*0.05, (50, 50, 200)), ((max as f64)*0.1 as f64, (50, 50, 255)), ((max as f64)*0.3 as f64, (200, 200, 0)), ((max as f64) * 0.8, (200, 0, 0))];
    let color_range = vec![((max as f64)*0.05, (0, 0, 255)), ((max as f64) * 0.8, (255, 0, 0))];
    let height: u16 = (step_size as u16) * i + min;
    let rgb = linear_color_interpolation(height as f64, &color_range);
    let (r, g, b) = rgb;
    let color = rgb_triple_to_hex_code(rgb);
    //eprintln!("height {:?} - color {:?}", height, color);
    if height <= max.into() && height >= min.into(){
      contour_level(&field, height).into_iter().map(|v| (color.clone(), simplify_with_eps(&v, 2.0))).collect()
    } else {
      vec![]
    }
  })
  .reduce(
    || vec![],
    |mut a, b| {
      a.extend(b);
      a
    });
  //eprintln!("min {:?}", min);
  //eprintln!("max {:?}", max);
  let document = create_contour_svg(&paths, w, h, 1.0);
  svg::save(out_path, &document).unwrap();
  Ok(())
}

fn bmp_to_jpg_with_quality(in_path: &str, out_path: &str, quality: u8)-> Result<(), ImageError>{
  let img = image::open(in_path)?;
  let mut buf = Vec::new();
  let mut encoder = image::codecs::jpeg::JpegEncoder::new_with_quality(&mut buf, quality);
  encoder.encode(img.as_rgb8().unwrap(), img.width(), img.height(), image::ColorType::Rgb8);
  let out_image = image::load_from_memory_with_format(&buf, image::ImageFormat::Jpeg)?;
  out_image.save(out_path);
  Ok(())
}