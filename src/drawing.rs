use svg::Document;
use svg::node::element::Path;
use svg::node::element::path::Data;


pub fn create_contour_svg(paths: &Vec<(String, Vec<(f64, f64)>)>, width: u32, height: u32, stroke_width: f64) -> Document {
  let mut document = Document::new()
      .set("viewBox", (0, 0, width, height))
      .set("width", width)
      .set("height", height);
  for (stroke, path) in paths {
    let mut data = Data::new();
    let mut is_first = true;
    let first = path[0];
    let last = path[path.len() - 1];
    for point in path {
      if is_first {
        data = data.move_to((point.0, point.1));
        is_first = false;
        //println!("point {:?}", point);
      } else {
        //println!("point {:?}", point);
        data = data.line_to((point.0, point.1));
      }
    }
    if first == last {
      data = data.close();
    }
    //eprintln!("color {:?}", stroke);
    let path = Path::new()
        .set("fill", "none")
        .set("stroke", stroke.clone())
        .set("stroke-width", stroke_width)
        .set("d", data);
    document = document.add(path);
  }
  document
}

// just build a linear interpolation you nonce
pub fn linear_red_blue(min: f64, max: f64, height: f64) -> (u64, u64, u64) {
  let frac = (height - min) as f64 / (max - min) as f64;
  let red = u64::max(0, ((2.0 * frac - 1.0) * 255.0) as u64);
  let green = (f64::sqrt(frac) * 50.0) as u64;
  let blue = u64::max(0, ((0.5 + 0.5 * frac) * 255.0) as u64);
  (red, green, blue)
}

pub fn linear_color_interpolation(input_value: f64, value_color_pairs: &Vec<(f64, (u8, u8, u8))>) -> (u8, u8, u8) {
  let mut low = value_color_pairs[0];
  for (i, &elem) in value_color_pairs.iter().enumerate() {
    let (high_reference_value, (red, green, blue)) = elem;
    if input_value < high_reference_value {
      if i == 0 { return (red, green, blue); }
      let (low_reference_value, (last_red, last_green, last_blue)) = low;
      if high_reference_value > low.0 {
        let mut factor = (input_value - low_reference_value) / (high_reference_value - low_reference_value);
        return (
          (last_red as i32 + ((red  as f64 - last_red  as f64) * factor) as i32) as u8,
          (last_green as i32 + ((green  as f64 - last_green  as f64) * factor) as i32) as u8,
          (last_blue as i32 + ((blue  as f64 - last_blue as f64 ) * factor) as i32) as u8,
        );
      }
    }
    low = elem;
  }
  low.1
}

// just testing tests )))
#[cfg(test)]
mod tests {
  // importing names from outer (for mod tests) scope.
  use super::*;

  #[test]
  fn test_linear_color_interpolation_below() {
    let colors = vec![(0.0, (0, 0, 0)), (1.0, (3, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(-3.4, &colors);
    assert_eq!((r, g, b), (0, 0, 0));
  }

  #[test]
  fn test_linear_color_interpolation_above() {
    let colors = vec![(0.0, (0, 0, 0)), (1.0, (3, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(42.4333333, &colors);
    assert_eq!((r, g, b), (3, 4, 0));
  }

  #[test]
  fn test_linear_color_interpolation_middle_1() {
    let colors = vec![(0.0, (0, 0, 0)), (1.0, (3, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(2.0, &colors);
    assert_eq!((r, g, b), (3, 2, 0));
  }

  #[test]
  fn test_linear_color_interpolation_middle_rounding_down() {
    let colors = vec![(0.0, (0, 0, 0)), (1.0, (3, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(0.5, &colors);
    assert_eq!((r, g, b), (1, 0, 0));
  }
  #[test]
  fn test_linear_color_interpolation_two_elem() {
    let colors = vec![(0.0, (0, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(2.0, &colors);
    assert_eq!((r, g, b), (2, 2, 0));
  }
  #[test]
  fn test_linear_color_interpolation_two_max() {
    let colors = vec![(0.0, (0, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(3.0, &colors);
    assert_eq!((r, g, b), (3, 4, 0));
  }
  #[test]
  fn test_linear_color_interpolation_two_min() {
    let colors = vec![(0.0, (0, 0, 0)), (3.0, (3, 4, 0))];
    let (r, g, b) = linear_color_interpolation(0.0, &colors);
    assert_eq!((r, g, b), (0, 0, 0));
  }#[test]
  fn test_linear_color_interpolation_broken_example() {
    let min = 161;
    let max = 65535;
    let colors = vec![((min as f64) as f64, (0, 0, 0)), ((max as f64)*0.05, (0, 0, 100)), ((max as f64)*0.3 as f64, (0, 0, 255)), ((max as f64) * 0.8, (0, 255, 0))];
    let (r, g, b) = linear_color_interpolation(51428.0, &colors);
    assert_eq!((r, g, b), (0, 247, 8));
  }

}

pub fn rgb_triple_to_hex_code(rgb: (u8, u8, u8)) -> String {
  let (r, g, b) = rgb;
  return format!("#{:02X}{:02X}{:02X}", r, g, b);
}

#[cfg(test)]
#[test]
fn test_rgb_triple_to_hex_code() {
  assert_eq!(rgb_triple_to_hex_code((255, 255, 255)), "#FFFFFF");
  assert_eq!(rgb_triple_to_hex_code((255, 0, 0)), "#FF0000");
  assert_eq!(rgb_triple_to_hex_code((0, 255, 0)), "#00FF00");
  assert_eq!(rgb_triple_to_hex_code((0, 0, 255)), "#0000FF");
  assert_eq!(rgb_triple_to_hex_code((0, 0, 0)), "#000000");
}
